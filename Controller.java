package com.app.Controller;

import com.app.Domain.Sensors;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
//creating RestController
@RestController
public class Controller
{
    @Autowired
    Receiver receiver;

    //autowired the Sender class
    @Autowired
    Sender sender;

    @GetMapping("/sensors")
    private void receiveRequest()
    {
        receiver.receive("r2c ");
    }

    @PostMapping("/sensors")
    private int saveSensors(@RequestBody Sensors sensors)
    {
        //sending request
        sender.send(""+
                sensors.getId()+"-"+
                sensors.getRoom()+"-"+
                sensors.getName()+"-"+
                sensors.getSenstype());
        return 0;
    }
}
